public class MergeSortInPlace {
    public static void sort(int[] array) {
        mergeSort(array, 0, array.length - 1);
    }

    private static void mergeSort(int[] array, int start, int end) {
        // Terminal Condition Otherwise
        if (start < end) {
            // Avoids Overflow
            int mid = start + (end - start) / 2;

            mergeSort(array, start, mid);
            mergeSort(array, mid+1, end);

            merge(array, start, mid, end);
        }
    }

    // Merges two subarrays of arr[]
    // First subarray is arr[l..m]
    // Second subarray is arr[m+1..r]
    private static void merge(int[] array, int l, int m, int r) {
        int secondPointer = m + 1;

        while (l <= m && secondPointer <= r) {
            if (array[l] <= array[secondPointer]) {
                l++;
            } else {
                // Shift Everything From Last Inserted Place (l) To secondPointer To Right
                int index = secondPointer;
                // Clone Item at secondPlace To Insert Before Shifted Items
                int value = array[index];
                while (index > l) {
                    array[index] = array[index-1];
                    index--;
                }
                array[l] = value;

                // Increment Values
                l++;
                m++;
                secondPointer++;
            }
        }
    }
}
