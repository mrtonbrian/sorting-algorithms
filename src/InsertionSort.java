public class InsertionSort {
    public static void sort(int[] array) {
        for (int i = 1; i < array.length; i++) {
            insert(array, i - 1, array[i]);
        }
    }

    private static void insert(int[] array, int rightIndex, int value) {
        int i;
        for (i = rightIndex; i >= 0 && array[i] > value; i--) {
            array[i + 1] = array[i];
        }
        array[i + 1] = value;
    }
}
