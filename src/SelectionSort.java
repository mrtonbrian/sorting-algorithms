public class SelectionSort {
    public static void sort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int minVal = array[i];
            int minValInd = i;

            for (int j = i; j < array.length; j++) {
                if (array[j] < minVal) {
                    minVal = array[j];
                    minValInd = j;
                }
            }

            int temp = array[i];

            array[i] = minVal;
            array[minValInd] = temp;
        }
    }
}
