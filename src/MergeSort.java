import java.util.Arrays;

public class MergeSort {
    public static void sort(int[] array) {
        int ind = 0;
        for (int i : mergeSort(array)) {
            array[ind] = i;
            ind++;
        }
    }

    private static int[] mergeSort(int[] array) {
        if (array.length == 1) {
            return array;
        }

        int[] halfA = mergeSort(Arrays.copyOfRange(array, 0, array.length/2));
        int[] halfB = mergeSort(Arrays.copyOfRange(array, array.length/2, array.length));

        return merge(halfA, halfB);
    }

    private static int[] merge(int[] list1, int[] list2) {
        // Declares Indices
        int list1Pointer = 0;
        int list2Pointer = 0;

        // Creates New List
        int list3[] = new int[list1.length + list2.length];
        // Continues To Search While Not At The End Of Either List

        while (list1Pointer < list1.length && list2Pointer < list2.length) {
            // If Item From List1 Is Less Than Item Of List 2
            if (list1[list1Pointer] < list2[list2Pointer]) {
                // Add Item From List1 To Output List
                list3[list1Pointer + list2Pointer] = list1[list1Pointer++];
                // Otherwise
            } else {
                // Add Item From List2 (Which Is Smaller) To Output List
                list3[list1Pointer + list2Pointer] = list2[list2Pointer++];
            }
        }

        // Add Remaining Items From List1
        while (list1Pointer < list1.length) {
            list3[list1Pointer + list2Pointer] = list1[list1Pointer];
            list1Pointer++;
        }

        // Add Remaining Items From List2
        while (list2Pointer < list2.length) {
            list3[list1Pointer + list2Pointer] = list2[list2Pointer];
            list2Pointer++;
        }

        return list3;
    }
}
