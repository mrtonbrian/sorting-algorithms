public class QuickSort {
    public static void sort(int[] array) {
        quickSort(array, 0, array.length-1);
    }

    private static void quickSort(int[] array, int p, int r) {
        if (p < r) {

            int p1 = partition(array, p, r);

            quickSort(array, p, p1 - 1);
            quickSort(array, p1 + 1, r);
        }
    }

    private static int partition(int[] array, int p, int r) {
        int pivot = array[r];

        int q = p;
        for (int j = p; j < r; j++) {

            if (array[j] <= pivot) {
                swap(array, j, q);
                q++;
            }
        }

        swap(array, q, r);
        return q;
    }

    private static void swap(int[] array, int a, int b) {
        int temp = array[b];

        array[b] = array[a];
        array[a] = temp;
    }
}
