import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        System.out.println("Generating Array");
        int[] array = generateRandomArray(1000000);
        System.out.println("Cloning Array For Builtin Sort");
        int[] cloneForBuiltin = array.clone();

        System.out.println("Cloning Array For Merge Sort");
        int[] cloneForMergeSort = array.clone();

        System.out.println();

        System.out.println("Starting Sorts");
        long startTest = System.currentTimeMillis();
        Arrays.sort(cloneForBuiltin);
        long endTest = System.currentTimeMillis();
        System.out.printf("Finished Builtin Sort (%.3f Seconds)\n", (endTest - startTest) / 1000.);

        long startQuicksort = System.currentTimeMillis();
        QuickSort.sort(array);
        long endQuicksort = System.currentTimeMillis();
        assert (Arrays.equals(cloneForBuiltin, array));
        System.out.printf("Finished Quicksort (%.3f Seconds)\n", (endQuicksort-startQuicksort)/ 1000.);

        long startMergeSort = System.currentTimeMillis();
        MergeSort.sort(cloneForMergeSort);
        long endMergeSort = System.currentTimeMillis();
        assert(Arrays.equals(cloneForBuiltin, cloneForMergeSort));
        System.out.printf("Finished Mergesort (%.3f Seconds)\n", (endMergeSort - startMergeSort) / 1000.);
    }

    private static int[] generateRandomArray(int size) {
        int[] array = new int[size];
        Random randomGenerator = new Random();  // Random number generator

        for (int i = 0; i < size; i++) {
            array[i] = i+1;
        }

        for (int i=0; i<array.length; i++) {
            int randomPosition = randomGenerator.nextInt(array.length);
            int temp = array[i];
            array[i] = array[randomPosition];
            array[randomPosition] = temp;
        }

        return array;
    }

    public static void printArray(int[] array) {
        System.out.print("{");

        for (int i = 0; i < array.length - 1; i++) {
            System.out.printf("%d, ", array[i]);
        }

        System.out.printf("%d}\n", array[array.length - 1]);
    }
}
